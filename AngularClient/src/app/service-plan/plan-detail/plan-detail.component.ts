import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs';
import { TS, PlanService} from '../service-plan.service';
import { switchMap } from '../../../../node_modules/rxjs/operators';
import { ActivatedRoute, ParamMap} from '@angular/router';
@Component({
  selector: 'app-plan-detail',
  templateUrl: './plan-detail.component.html',
  styles: []
})
export class PlanDetailComponent implements OnInit,OnDestroy {
  streams$ : Observable<TS[]>;
  sub: any;
  planName:string
  constructor(
    private service: PlanService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log("Plan detail init.")
    this.sub = this.route.params.subscribe(
      params => {
        console.log(params)
        this.planName = params.name
      }
    )
    //this.streams$ = this.route.paramMap.pipe(
    //  switchMap(
    //    (params: ParamMap)=>{
    //      console.log("Plan deail -> switchMap",params)
    //      return this.service.getStreams();
    //    }
    //  )
    //)
  }
   ngOnDestroy(){
     this.sub.unsubscribe();
   }

}
