import { Injectable } from '@angular/core';
import { of, Observable,forkJoin } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

export class Plan{
  constructor(public id:number,public name:String, public data:any[]){}
}

export class Service{
  constructor(public id:number,public name:String, public data:any[]){}
}

export class TS{
  constructor(public id:number,public name:String, public data:any[]){}
}
export class Preset{
  constructor(public id:number,public plan:string
    , public preset_name:string,public preset_value:PresetValue[]){}
};
export class PresetValue{
  //public id:number; public name:string;
  constructor(public id:number, public name:string){}
}
export class PresetDetailOption{
  constructor(public id:number,public name:string,public checked:boolean){}
}
const PLANS = [
  new Plan(0,"dstv",[])
  ,new Plan(1,"catv",[])
];

const STREAMS = [
  new TS(0,"ts1",[])
  ,new TS(1,"ts2",[])
  ,new TS(2,"ts3",[])
]
 
const SERVICE = [
  new Service(1,'Channel 01',[])
  ,new Service(2,'Channel 02',[])
]


@Injectable({
  providedIn: 'root'
})
export class PlanService {
  dstvPlan:any[];
  private dstvPlanUrl = 'http://localhost:3000/';
  private planDataUrl = 'http://localhost:3000/';
  private dstvPlanUpdateUrl = 'http://localhost:3000/dstv/update';
  private viewsByPlan = 'http://localhost:3000/viewsbyplan';
  private viewPreset = 'http://localhost:3000/view';
  private viewRename = 'http://localhost:3000/renameview';
  private viewName = 'http://localhost:3000/viewname';
  //private viewPresetUpdate = 'http://localhost:3000/view/';
  private planHeaders = 'http://localhost:3000/plan-header';
  constructor(
    private http:HttpClient
  ) { }

  getAllPlanHeaders(){
    return this.http.get<PresetValue[]>(`${this.planHeaders}`)
      .pipe(
        tap(result=>this.log("get all header complete"))
      )
  }
  getPresetValueOption(currentPreset:Preset):Observable<any>{
    const allHeader =this.getAllPlanHeaders()
    const viewPresetValue = this.getViewPresetValues(currentPreset)
    return forkJoin(allHeader,viewPresetValue);
  }

  getViewById(preset:Preset):Observable<any>{
    const presetName = this.http.get<any[]>(`${this.viewName}/${preset.id}`)
    const presetValue = this.getViewPresetValues(preset)
    return forkJoin(presetName,presetValue)
  }

  getViewPresetNames(planName:string){
    return this.http.get<Preset[]>(`${this.viewsByPlan}/${planName}`)
      .pipe(
        tap(result=>this.log("get view preset: "+planName+" :complete")),
        catchError(this.handleError('getViewPreset',[]))
      )

  }

  //getViewPresetValues(planName:string, id:number){
  getViewPresetValues(currentPreset:Preset){
    return this.http.get<any[]>(`${this.viewPreset}/${currentPreset.plan}/${currentPreset.id}`)
      .pipe(
        tap(result=>this.log(`get view preset value:${currentPreset.plan}/${currentPreset.id} :complete`))
      )
  }

  postNewView(newPreset:Preset){
    return this.http.post<any>(`${this.viewPreset}`,newPreset)
      .pipe(
        tap((lastId)=>{
          console.log("Last id: ",lastId);
        })
      );
  }

  postRenameView(viewPreset:Preset){
    return this.http.post<any>(`${this.viewRename}/${viewPreset.id}`
      ,viewPreset
    ).pipe(
        tap((results)=>{
          console.log("Rename view- complete",results)
        })
      )
  }

  deleteView(viewId:number){
    return this.http.delete<any>(`${this.viewPreset}/${viewId}`)
      .pipe(
        tap((res)=>{
          console.log("delete ",res)
        })
      )
  }

  //updateViewPresetDetail(planName:String,viewId:number,presetDetailList:any){
  updateViewPresetDetail(currentPreset:Preset,presetDetailList:any){
    return this.http.post(
      //`${this.viewPreset}/${currentPreset.plan}/${currentPreset.id}`
      `${this.viewPreset}/${currentPreset.id}`
      ,presetDetailList
    ).pipe(
        tap(result=>this.log(`update view ${currentPreset.id}- complete`))
      )
  }

  getPlans(){
    return  of(PLANS);
  }
  getPlan(name:string){
    console.log("getting plan")
    return  of(PLANS);
  }
  getService(){
    return of(SERVICE);
  }
  getPlanData(planName:String): Observable<any[]>{
    return this.http.get<any[]>(this.planDataUrl+planName)
      .pipe(
        tap(dstvPlan=>this.log("get dstvPlan complete")),
        catchError(this.handleError('getDstv',[]))
      )
  }
  getPlanDataTest(): Observable<any[]>{
    return this.http.get<any[]>(this.dstvPlanUrl)
      .pipe(
        tap(dstvPlan=>this.log("get dstvPlan complete")),
        catchError(this.handleError('getDstv',[]))
      )
  }
  private log (message:any):void{
    console.log(message);
  }
    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
