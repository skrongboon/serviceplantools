import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan',
  template: `
  <div class="container-fluid">
    <p>
      Service plan
    </p>
  </div>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class PlanComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
