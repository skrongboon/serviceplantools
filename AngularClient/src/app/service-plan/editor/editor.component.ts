import { Component, OnInit, Input,OnChanges,SimpleChange } from '@angular/core';
import {DstvPlanService} from '../../dstv-plan.service';
import {HotTableRegisterer} from '@handsontable/angular';
import * as Handsontable from 'handsontable';
import { Observable } from '../../../../node_modules/rxjs';
import { TS,Service, PlanService} from '../service-plan.service';
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnChanges {
  @Input() planName:String;
  change: any[];
  dstvHeader: any[];
  planData : any;
  fetched = false;
  error = false;
  columns:any[]=[];
  HOTsettingsObj = {
    startRows:100,
    startCols:300,
    manualColumnResize:true,
    minSpareRows:100,
    minSpareCols:10,
    rowHeaders: true,
    colHeaders:true,
    contextMenu:true,
    autoColumnSize:{useHeaders:true},
    afterRenderer:function(instance, td, row, col, prop, value, cellProperties){
      td.style.textAlign = 'center';
      if(row === 0){
        td.style.background = '#ddf2c9';
        td.style.fontWeight = 'bold'; 
      }
      //cellProperties.numericFormat = {pattern: '0.[00]%', culture: 'de-DE'}
      Handsontable.renderers.NumericRenderer.apply(this, arguments);
    }
  }
  private instanceId = "hot";
  private hotInstance :Handsontable;
 
  constructor(
    private service: PlanService,
    private hotRegisterer: HotTableRegisterer)   { }
  getPlanData(planName:string):void{
    console.log("Getting plan:",planName);
    this.service.getPlanData(planName)
        .subscribe(
          data => this.planData =data,
          err => this.error = true,
          () => this.fetched = true,
        );
  }
  ngOnChanges(changes:{[propKey:string]: SimpleChange}) {
    let planName;
    for (let propName in changes) {
      let changedProp = changes[propName];
      let to = JSON.stringify(changedProp.currentValue);
      planName = changedProp.currentValue;
      if (changedProp.isFirstChange()) {
        console.log(`Initial value of ${propName} set to ${to}`);
      } else {
        let from = JSON.stringify(changedProp.previousValue);
        console.log(`${propName} changed from ${from} to ${to}`)
      }
    }
    this.getPlanData(planName);
    console.log("Editor onchange")
    /*Get plan data from service */
  }

}
