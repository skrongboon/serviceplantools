import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap} from '@angular/router';
import { Observable } from '../../../node_modules/rxjs';
import { Plan, PlanService} from './service-plan.service'
import { switchMap } from '../../../node_modules/rxjs/operators';

@Component({
  selector: 'app-plan-list',
  template: `
  <div class="container-fluid">
    <ul class="nav nav-pills">
      <li class="nav-item" *ngFor="let plan of plans$ | async"
        [class.selected]="plan.name === selectedId">
        <a class="nav-link"  [routerLink]="[plan.name]"
        routerLinkActive="active"
        >
          {{ plan.name }}
        </a>
      </li>
    </ul>
  </div>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class PlanListComponent implements OnInit {
  plans$: Observable<Plan[]>;
  selectedId: number;

  constructor(
    private service : PlanService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.plans$ = this.route.paramMap.pipe(
      switchMap(
        (params:ParamMap)=>{
          console.log("Plan list:",params)
          this.selectedId = +params.get('id')
          return this.service.getPlans();
        }

      )
    )
  }

}
