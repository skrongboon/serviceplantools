import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { ServicePlanRoutingModule } from './service-plan-routing.module';
import { HotTableModule } from '@handsontable/angular';
import { PlanComponent} from './plan.component';
import { PlanHomeComponent} from './plan-home.component';
import { PlanListComponent} from './plan-list.component';
import { PlanDetailComponent } from './plan-detail/plan-detail.component';
import { EditorComponent } from './editor/editor.component';
import { PlanViewComponent } from './plan-view/plan-view.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    HotTableModule,
    NgbModule,
    FormsModule,
    AngularFontAwesomeModule,
    ServicePlanRoutingModule,
  ],
  declarations: [
    PlanComponent,
    PlanHomeComponent,
    PlanListComponent,
    PlanDetailComponent,
    EditorComponent,
    PlanViewComponent,
  ]
})
export class ServicePlanModule { }
