import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { PlanService,Preset,PresetValue,PresetDetailOption} from '../service-plan.service';
import { ControlValueAccessor } from '../../../../node_modules/@angular/forms';
import { tap } from '../../../../node_modules/rxjs/operators';
import { headersToString } from '../../../../node_modules/@types/selenium-webdriver/http';

//export class PresetNameDom{
//  constructor(
//    public currentId:number
//    ,public input_editName:string
//  ){}
//}

@Component({
  selector: 'app-plan-view',
  templateUrl: './plan-view.component.html',
  styleUrls: ['./plan-view.component.css']
})


export class PlanViewComponent implements OnInit {
  @Input() planName:string;
  /*Preset name list */
  private addNewPresetClicked:boolean = false;  
  private input_newPresetName:string;
  private presetNameDom:Preset = new Preset(null,null,null,null);
  /*Current Preset */
  private currentPreset:Preset = new Preset(null,null,null,null);
  /* Preset list */
  private presetNameList:Preset[]
  /*Current Preset detail list */
  private addPresetValueClicked:boolean = false;
  private presetDetailList: PresetDetailOption[];//Model for Preset detail dom

  constructor(
    private service:PlanService,
    private route:ActivatedRoute
  ) { }

  onAddNewPresetClick(){
    this.presetDetailList = null;
    this.addNewPresetClicked = this.addNewPresetClicked==true?false:true;
  }
  
  onAddNewPresetCancleClick(){
    this.addNewPresetClicked = false;
    this.input_newPresetName =null;
  }
  
  onAddNewPresetSaveClick(){
    //console.log("save new preset clicked")
    let newPreset:Preset = new Preset(null,this.planName,this.input_newPresetName,[])
    this.presetNameList.push(newPreset);
    this.addNewPresetClicked=false;
    this.input_newPresetName =null;
    /*ToDO get id from db and add to this preset object */
    this.postNewView(newPreset)
  }

  onPresetNameDelete(presetId:number){
    console.log("onPresetNameDelete",presetId);
    this.deleteView(presetId)
  }

  onPresetValueChange(event: any){
    let headerId = event.target.value
    let found = this.presetDetailList.find((header)=>{
      return header.id == headerId
    })
    if(found.checked == true){
      found.checked = false;
    }else{
      found.checked = true;
    }
    /* call service for update presetDetail */
    this.updatePresetDetailList()
    //console.log(found);
  }

  getViewPresetNames(planName:string){
    this.service.getViewPresetNames(planName)
      .subscribe(
        (result:Preset[])=>{
          this.presetNameList=result
        }
      )

  }

  getViewPresetValueOptionList(currentPreset:Preset){
    this.service.getPresetValueOption(currentPreset)
      .subscribe(
        (res)=>{
          console.log("TEST CONCAT OBSERVABLE =>",res)
          let allHeaders:PresetValue[] = res[0],
              presetValueList:PresetValue[] = res[1]
          this.createPresetDetailOptionList(allHeaders,presetValueList);
        }
      )
  
  }

  updatePresetDetailList(){
    let changes = this.presetDetailList.filter(
      (preset)=>{
        return preset.checked == true;
      }
    );
    let changeIdsList = changes.map(
      (change)=>{
        return change.id
      }
    )
    console.log("Current view:", this.currentPreset)
    this.service.updateViewPresetDetail(
      this.currentPreset
      ,changeIdsList
    ).subscribe((res)=>{
        console.log("Update-complete Preset:",this.currentPreset)
      });
  }
  
  postNewView(newPreset:Preset){
    this.service.postNewView(newPreset)
    .subscribe(
      (results)=>{
        console.log(`Result from add new view: ${results}`)
        newPreset.id = results.insertId;
      }
    );

  }

  deleteView(viewId:number){
    this.service.deleteView(viewId)
      .subscribe(
        (result)=>{
          this.presetNameList.splice(
            this.presetNameList.findIndex(
              (preset)=>{
                return preset.id == viewId
              }
            )
            ,1)
        }
      );
  }

  postRenameView(preset:Preset){
    this.service.postRenameView(preset)
      .subscribe(
        (result)=>{
          console.log("Rename-complete:",result)
          this.presetNameList.forEach(
            (view)=>{
              if(view.id == preset.id){
                view.preset_name = preset.preset_name
              }
            }
          )
          this.presetNameDom.id = null;
          this.presetNameDom.preset_name = null;
        }
      )
  }

  /*Componet Event */
  addPresetValue(){
    //console.log("add preset value");
    this.addPresetValueClicked = (this.addPresetValueClicked)?false:true;
  }

  onSelectPreset(presetId:number){
    //this.viewPresetCurrentId = presetId;
    this.currentPreset = this.presetNameList.find((preset)=>{
      return preset.id==presetId;
    })
    console.log(`Select preset click,`, this.currentPreset);
    this.getViewPresetValueOptionList(this.currentPreset);
  }

  createPresetDetailOptionList(allHeaders:PresetValue[]
    ,presetValueCurrentList:PresetValue[])
  {
    this.presetDetailList = [];
    allHeaders.forEach((value)=>{
      let checked:boolean = false;
      presetValueCurrentList.forEach(
        (cur)=>{
          if(cur.id === value.id){
            /* found */
            checked = true;
            return ;
          }
        }
      )
      this.presetDetailList.push(new PresetDetailOption(value.id,value.name,checked)) 
    });
  
  }
  showRenameForm(presetId:number){
    return (presetId === this.presetNameDom.id)?true: false;
  }
  onRenamePresetClick(presetId:number){
    this.presetNameDom.id 
      = (this.presetNameDom.id==presetId)?null:presetId;
    //this.presetNameDom.preset_name = null
    this.presetNameList.forEach(
      (name)=>{
        if(name.id === presetId){
          this.presetNameDom.preset_name = name.preset_name
        }
      }
    )
  }
  onRenamePresetSaveClick(presetId:number){
    this.presetNameDom.id = presetId;
    console.log("Edit preset name:",this.presetNameDom.preset_name)
    this.postRenameView(this.presetNameDom)
  }
  onRenamePresetCancleClick(presetId:number){
    this.presetNameDom.id = null;
    this.presetNameDom.preset_name = null 
  }

  ngOnInit() {
    this.planName = this.route.snapshot.params.name
    this.getViewPresetNames(this.planName);
    console.log("presetNameDom:",this.presetNameDom)
  }

}
