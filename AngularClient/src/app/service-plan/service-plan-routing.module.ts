import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanComponent } from './plan.component'
import { PlanHomeComponent } from './plan-home.component'
import { PlanListComponent } from './plan-list.component'
import { PlanDetailComponent } from './plan-detail/plan-detail.component'
import { PlanViewComponent } from './plan-view/plan-view.component'

const routes: Routes = [
  //{
  //  path:'plan',redirectTo:"/serviceplan/dstv"

  //},
  {
    path:'plan',component:PlanComponent,
    children:[
      {
        path:'', component:PlanListComponent
        ,children:[
          {
            path:':name',component:PlanDetailComponent
          },
          {
            path:':name/viewedit',component:PlanViewComponent
          }
          ,{
            /* Default route of PlanList*/
            path:'',component:PlanHomeComponent
          }
        ]  
      }

    ]
  },
  //{
  //  path:'serviceplan/:name',component:PlanComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicePlanRoutingModule { }
