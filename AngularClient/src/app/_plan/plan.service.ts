import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

export class Plan{
  constructor(public id:number, public data:String){}
}

const PLANS = [
  new Plan(1,"test"),
  new Plan(2,"test2")
];

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor() { }
  gerServicePlan(){
    return  of(PLANS);
  }
  getServicePlan(name:string){
    console.log("getting plan")
    return  of(PLANS);
  }
}
