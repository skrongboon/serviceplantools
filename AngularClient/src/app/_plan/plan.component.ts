import { Component, OnInit } from '@angular/core';
import {DstvPlanService} from '../dstv-plan.service';
import {HotTableRegisterer} from '@handsontable/angular';
import * as Handsontable from 'handsontable';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Plan, PlanService} from './plan.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {
  private serviceName$: Observable<String>;
  servicePlan$: Observable<Plan[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: PlanService,
    private dstvPlanService: DstvPlanService
    ,private hotRegisterer: HotTableRegisterer) {}
  
  ngOnInit() {
    console.log("Plan component init.",this.route.snapshot.paramMap.get('name'))
    //this.serviceName = this.route.snapshot.paramMap.get('name');
  }
  


}
