import { Injectable } from '@angular/core';
import { Service } from './model/model.service';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class DstvPlanService {
  dstvPlan:any[];
  private dstvPlanUrl = 'http://localhost:3000/';
  private dstvPlanUpdateUrl = 'http://localhost:3000/dstv/update';
  constructor(
    private http:HttpClient){ }

  getDstv(): Observable<any[]>{
    return this.http.get<any[]>(this.dstvPlanUrl)
      .pipe(
        tap(dstvPlan=>this.log("get dstvPlan complete")),
        catchError(this.handleError('getDstv',[]))
      )
  }
  private log (message:any):void{
    console.log(message);
  }

  updateDstvPlan(dstvPlan:any[]): void{
    //this.dstvPlan = dstvPlan;
    console.log("update",dstvPlan)
    this.http.post(this.dstvPlanUpdateUrl,dstvPlan)
    .subscribe(
      res=>{console.log(res)},
      err=>{console.log("error")}
    );
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
