import { Component, OnInit } from '@angular/core';
import {Service } from '../model/model.service';  
import {DstvPlanService} from '../dstv-plan.service';
import {HotTableRegisterer} from '@handsontable/angular';
import * as Handsontable from 'handsontable';
import { RouterLinkWithHref } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  change: any[];
  dstvHeader: any[];
  dstvPlan : any;
  fetched = false;
  error = false;
  columns:any[]=[];
  HOTsettingsObj = {
    startRows:100,
    startCols:300,
    manualColumnResize:true,
    minSpareRows:100,
    minSpareCols:10,
    rowHeaders: true,
    colHeaders:true,
    contextMenu:true,
    autoColumnSize:{useHeaders:true},
    afterRenderer:function(instance, td, row, col, prop, value, cellProperties){
      td.style.textAlign = 'center';
      if(row === 0){
        td.style.background = '#ddf2c9';
        td.style.fontWeight = 'bold'; 
      }
      //cellProperties.numericFormat = {pattern: '0.[00]%', culture: 'de-DE'}
      Handsontable.renderers.NumericRenderer.apply(this, arguments);
    }
  }
  private instanceId = "hot";
  private hotInstance :Handsontable;
  constructor(
    private dstvPlanService: DstvPlanService
    ,private hotRegisterer: HotTableRegisterer) {}
  
  getDstv():void{
    this.dstvPlanService.getDstv()
        .subscribe(
          data => this.dstvPlan =data,
          err => this.error = true,
          () => this.fetched = true,
        );
  }
  onSave(hotInstace):void{
    console.log("click")
    this.hotInstance = hotInstace;
    let sourceData = this.hotInstance.getSourceData();
    //console.log(this.hotInstance.getData())
    //console.log(this.hotInstance.getCell(0,0))
    //console.log(this.hotInstance.getCellMeta(0,0))
    console.log(sourceData)
    this.dstvPlanService.updateDstvPlan(sourceData);
  }

  initHotData():void{
    this.dstvPlan = [{ID:1,TSID:1
    }]
  }
  afterLoadData(){
    console.log("after load data")
  }
  addColumn(newColumnName){
    //console.log("addind column:",newColumnName)
    /*change hot-table column */
    //this.hotColumns.push({data:newColumnName,title:newColumnName});
    /*change hot-table header */
    //this.hotColumnsHeader.push(newColumnName);
    //this.hotInstance.render();
    //console.log("new column header:",this.hotInstance.getColHeader());
    console.log("source data :",this.hotInstance.getSourceData());
    this.dstvPlanService.updateDstvPlan(this.hotInstance.getSourceData());
  }

  ngOnInit() {
    console.log("ng on init");
    
    //this.initHotData();
    //this.dstvPlan = [[1,2]];
    //this.dstvPlan = [{fullname:1,b:1},{fullname:2,b:2}]
    this.getDstv();
  }
  afterHotInit = (hotInstance) => {
    console.log("after hot init")
    this.hotInstance = hotInstance;
    console.log("hotInstance:",hotInstance);
    //hotInstance.loadData(this.getDstv())
  }
  onAfterChange = (change) => {
    // for store changes
    //console.log(change.getSourceData())
  }

}
