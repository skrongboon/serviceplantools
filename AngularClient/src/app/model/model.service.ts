export class Service {
    ID : String;
    TSID : String;
    True_Physical_No : String;
    fullname : String;
    resolution : String;
    si_service_id : String;
    MPEG4_Number : String;
    number_of_audio_components : String;
    PCR_pid_dec : String;
    pmt_pid_dec : String;
    video_pid_dec : String;
    video_pid_hex_0x : String;
    audio_1_pid_dec : String;
    audio_1_pid_hex_0x : String;
    audio_1_lang : String;
    audio_1_coding_mode : String;
    audio_1_bitrate : String;
    audio_2_pid_dec : String;
    audio_2_pid_hex_0x : String;
    audio_2_lang : String;
    audio_2_coding_mode : String;
    audio_2_bitrate : String;
    audio_3_pid_dec : String;
    audio_3_pid_hex_0x : String;
    audio_3_lang : String;
    audio_3_coding_mode : String;
    audio_3_bitrate : String;
    data_1_pid_dec : String;
    data_pid_hex_0x : String;
    TVS_TS_ID : String;
    Thaicom_TP : String;
    BW_MHz : String;
    Freq_MHz : String;
    Symbol_rate : String;
    FEC_or_LDPC : String;
    Mod : String;
    audio_pair_id : String;
    NDS_ecm_pid_dec : String;
    NDS_ecm_pid_hex_0x : String;
    NDS_AC_ref_header : String;
    NDS_AC_ref_ecm_pid_hex : String;
    NDS_emm_pid : String;
    Irdeto_ecm_pid_dec : String;
    Irdeto_AC_pid_hex_0x : String;
    Irdeto_emm_pid : String;
    SCAS_ACAS : String;
    ABV_ecm_pid_dec : String;
    ABV_AC : String;
    ABV_emm_pid : String;
    


}