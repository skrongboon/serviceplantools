import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HotTableModule } from '@handsontable/angular';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
/* Self-defined Module,Component */
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
//import { HomeComponent } from './home/home.component';
import { ServicePlanModule } from './service-plan/service-plan.module';
import { NotFoundComponent } from './not-found.component';
import { FormsModule } from '../../node_modules/@angular/forms';
import { ViewComponent } from './view/view.component';
//import { PlanListComponent } from './service-plan/plan-list.component';
//import { PlanHomeComponent } from './service-plan/plan-home.component';
//import { PlanComponent } from './service-plan/plan.component';
//import { ServicePlanComponent } from './service-plan/service-plan.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NotFoundComponent,
    ViewComponent,
    //PlanListComponent,
    //PlanHomeComponent,
 //   PlanComponent,
    //ServicePlanComponent,
    //HomeComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpClientModule,
    HotTableModule.forRoot(),
    AngularFontAwesomeModule,
    ServicePlanModule,
    AppRoutingModule, /*AppRoutingModule Must be last order */

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(
  ){
  }
}