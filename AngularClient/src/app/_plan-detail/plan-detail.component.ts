import { Component, OnInit } from '@angular/core';
import {DstvPlanService} from '../dstv-plan.service';
import {HotTableRegisterer} from '@handsontable/angular';
import * as Handsontable from 'handsontable';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanDetailComponent implements OnInit {
  serviceName :String;
  ts: String;
  change: any[];
  dstvHeader: any[];
  dstvPlan : any;
  fetched = false;
  error = false;
  columns:any[]=[];
  HOTsettingsObj = {
    startRows:100,
    startCols:300,
    manualColumnResize:true,
    minSpareRows:100,
    minSpareCols:10,
    rowHeaders: true,
    colHeaders:true,
    contextMenu:true,
    autoColumnSize:{useHeaders:true},
    afterRenderer:function(instance, td, row, col, prop, value, cellProperties){
      td.style.textAlign = 'center';
      if(row === 0){
        td.style.background = '#ddf2c9';
        td.style.fontWeight = 'bold'; 
      }
      //cellProperties.numericFormat = {pattern: '0.[00]%', culture: 'de-DE'}
      Handsontable.renderers.NumericRenderer.apply(this, arguments);
    }
  }
  private instanceId = "hot";
  private hotInstance :Handsontable;
  constructor(
    private dstvPlanService: DstvPlanService
    ,private hotRegisterer: HotTableRegisterer
    ,private route: ActivatedRoute
    ,private router: Router
  ) {}
  
  getDstv():void{
    this.dstvPlanService.getDstv()
        .subscribe(
          data => this.dstvPlan =data,
          err => this.error = true,
          () => this.fetched = true,
        );
  }
  ngOnInit() {
    console.log("ng on init");
    console.log("Plan component init.",this.route.snapshot.paramMap.get('name'))
    this.serviceName = this.route.snapshot.paramMap.get('name');
    this.ts = this.route.snapshot.paramMap.get('ts');

  }
  back(){
    this.router.navigate(['/']);
  }
}
