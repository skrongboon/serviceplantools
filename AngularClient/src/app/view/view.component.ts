import { Component, OnInit } from '@angular/core';
import { HotTableRegisterer } from '@handsontable/angular';
import { Preset,PlanService, PresetValue} from  '../service-plan/service-plan.service';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-view',
  template: `
    <div class="row">
      <h5>
        {{preset.plan}}:{{preset.preset_name}}
      </h5>
    </div>
    <div class="row" >
      <div class="column">
        <hot-table
          [hotId]="instanceId"
          [data] = "planData" 
          [settings]= HOTsettingsObj
        >
          <hot-column *ngFor="let column of hotColumns"></hot-column>
        </hot-table>
      </div>
    </div>
  `,
  styles: []
})
export class ViewComponent implements OnInit {
  private preset:Preset;
  private instanceId = "hotView"
  private hotInstance: Handsontable;
  private planData;
  HOTSettingsObj = {}

  constructor(
    private route:ActivatedRoute,
    private service:PlanService,
    private hotRegisterer: HotTableRegisterer
  ) { }

  getViewById(viewId:number){
    this.service.getViewById(this.preset)
      .subscribe(
        (result)=>{
          console.log(result)
          let presetName:Preset = result[0]
          let presetValue:PresetValue[] = result[1]
          this.preset = presetName 
          this.preset.preset_value = presetValue;
        }
      )
  }

  ngOnInit() {
    let viewId:number = this.route.snapshot.params.id
    console.log(`ViewId: ${viewId}`)
    this.preset = new Preset(viewId,null,null,null)
    this.getViewById(this.preset.id)
  }

}
