import { TestBed, inject } from '@angular/core/testing';

import { DstvPlanService } from './dstv-plan.service';

describe('DstvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DstvPlanService]
    });
  });

  it('should be created', inject([DstvPlanService], (service: DstvPlanService) => {
    expect(service).toBeTruthy();
  }));
});
