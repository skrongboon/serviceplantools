import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found.component';
import { ViewComponent } from './view/view.component';

const routes: Routes = [
  //{
  //  path: '', redirectTo:'/serviceplan/dstv',pathMatch:'full'
  //},
  {
    path: 'plan'
    , loadChildren:'./service-plan/service-plan.module#ServicePlanModule' 
    ,data:{preload:true}
  },
  {
    path: 'view/:id', component:ViewComponent,
  },
  {
    path: ''
    ,redirectTo:'/plan',pathMatch:'full'
  },
  { path:'**',component:NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        //enableTracing:true
      }
     )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
