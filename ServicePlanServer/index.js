var express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser')

var app = express();
app.use(cors())
app.use(bodyParser.json({limit:'50mb'}))
app.use(bodyParser.urlencoded({ extended: true }))

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password     : 'ted@dm1n',
  database : 'service-plan',
  insecureAuth: true,
  multipleStatements:true
});

app.get('/datawithview/:planName/:viewId',(req,res)=>{
  let planName = req.params.planName,
  viewId = req.params.viewId,
  selectedHeader = "",
  rstPreset=[], rstPlanData=[], rstComplete=[];

  sqlViewPrest = `SELECT preset_value FROM view_preset where id = ${viewId}`;
  sqlPlanData = `SELECT JSON_EXTRACT(raw,${selectedHeader}) FROM ${planName} `;

  connection.query(""
    ,(err,results)=>{
      if(err) throw err;
      rstPreset = results[0]
      rstPlanData = results[1]
      selectedH
      res.send(rstComplete)
    });

})

app.get('/plan-header',(req,res)=>{
  let sql = "Select * from plan_header";
  connection.query(
    sql,(err,results)=>{
      if (err) throw err;
      res.send(results)
    }
  )
});

app.get('/viewsbyplan/:planName',(req, res)=>{
  console.log("Client request: "+req.url)
  let planName = req.params.planName
  let sql = "Select id,plan,preset_name from view_preset where plan = '"+planName+"' AND `delete` = 0";
  connection.query(
    sql
    ,(err,results)=>{
      if (err) throw err;
      res.send(results)
    }
  );
});

app.get('/viewname/:id',(req,res)=>{
  let sql = `SELECT id,plan,preset_name FROM view_preset where id = '${req.params.id}' LIMIT 1`;
  connection.query(
    sql,(err,result)=>{
      if(err) throw err;
      res.send(result[0]);

    }
  );
})

/*Get value by view id */
app.get('/view/:planName/:viewId',(req, res)=>{
  console.log("Client request: "+req.url)
  let planName = req.params.planName
  let id = req.params.viewId;
  let sql = `SELECT h.id as id,h.name as name FROM view_preset p
  RIGHT join plan_header h on JSON_CONTAINS(p.preset_value,CAST(h.id as CHAR),'$')
  WHERE  p.id =${id}`;
  connection.query(
    sql
    ,(err,results)=>{
      if (err) throw err;
      //let values = results.map((row)=>{
      //  return row.name
      //})
      res.send(results)
    }
  );
});

app.post('/view',(req,res)=>{
  let sql   = "INSERT INTO view_preset SET ?";
  console.log("post new view: ",req.body)
  let data  = {id:null,plan:req.body.plan,preset_name:req.body.preset_name}; 
  let insertId;
  connection.query(sql,data
    ,(err,results)=>{
      if (err) throw err;
      insertId = results.insertId
      console.log(insertId)
      res.send({insertId:insertId})
    }
  );
});

app.post('/view/:viewId',(req, res)=>{
//app.post('/view/:planName/:viewId',(req, res)=>{
  console.log("Client request: "+req.url)
  let viewId = req.params.viewId;
  let body = JSON.stringify(req.body);
  console.log(body)
  let sql =`UPDATE view_preset SET preset_value = '${body}' WHERE id = ${viewId}`;
  connection.query(
    sql,[body]
    ,(err,results)=>{
      if (err) throw err;
      res.send(results)
    }
  );
});

app.delete('/view/:viewId',(req,res)=>{
  console.log(`Client request: delete view `)
  let viewId = req.params.viewId;
  let sql = "UPDATE view_preset set `delete` = 1 WHERE id = ?"
  connection.query(
    sql,[viewId],(err, results)=>{
      if (err) throw err;
      res.send(results);
    }
  )

});
app.post('/renameview/:viewId',(req,res)=>{
  let viewId = req.params.viewId;
  console.log(`Reame-${viewId}`)
  let sql = 'UPDATE view_preset set preset_name = ? WHERE id = ?';
  let newName = req.body.preset_name;
  connection.query(
    sql,[newName,viewId],(err, result)=>{
      if (err) throw err;
      res.send(result);
    }

  )
})

app.get('/:planName', function (req, res) {
  //connection.query('select * from `satellite-test`', function (error, results) {
  //  if (error) throw error;
  //  res.send(results);
  //});
  console.log("Client request : "+req.params.planName)
  let planName = (req.params.planName!==undefined)?req.params.planName:"dstv";
  connection.query('select data from `'+planName+'`', function (error, results) {
    if (error) throw error;
    res.send(results.map((row)=>{
      return row.data.split(",");
    }));
  });
});

app.post('/dstv/update',(req,res)=>{
  var services  = req.body;
  let keys = "";
  let values = services.map((service,index)=>{
    return [index,service.toString()]
  });
  let updates = services.map((service)=>{
    return [service.toString()]
  });
  let sql = 'INSERT INTO `satellite` (ID, data) VALUES ? '
  +'ON DUPLICATE KEY UPDATE data = VALUES (?)';
  //console.log(updates)
  connection.query(sql, [values], function (error, results, fields) {
      if (error) throw error;
  });
  res.send(services)



});

app.post('/dstv/add',(req,res)=>{
    var services  = req.body;
    console.log(services)
    //var query = connection.query('INSERT INTO `satellite-test` SET ?', service, function (error, results, fields) {
    //    if (error) throw error;
    //});
    let keys = Object.keys(services[0]);
    let values = services.map( obj => keys.map( key => obj[key]));
    console.log(keys.join(','))
    let sql = 'INSERT INTO `satellite-test` (' + keys.join(',') + ') VALUES ?';
    connection.query(sql, [values], function (error, results, fields) {
        if (error) throw error;
    });
    res.send(services)
    //console.log(query.sql); // INSERT INTO posts SET `id` = 1, `title` = 'Hello MySQL'
    }
);


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});